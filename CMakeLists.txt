# Should work with 3.14 and through 3.18
cmake_minimum_required(VERSION 3.14...3.18)

# Project name and a few useful settings. Other commands can pick up the results
project(
  TabletopCityBuilder
  VERSION 0.1
  DESCRIPTION "An Augmented Reality tabletop game/tool for designing and simulating cities."
  LANGUAGES CXX)

# FetchContent added in CMake 3.11, downloads during the configure step
include(FetchContent)

find_package(PkgConfig REQUIRED)
find_package(OpenCV REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(ASSIMP REQUIRED)

# https://github.com/juliettef/GLFW-CMake-starter/blob/master/CMakeLists.txt
# https://discourse.glfw.org/t/issues-adding-glfw-source-to-a-cmake-project/1511/2
set(GLFW_BUILD_DOCS OFF CACHE BOOL  "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)

FetchContent_Declare(
  glfw
  GIT_REPOSITORY https://github.com/glfw/glfw.git
  GIT_TAG 3.3.2)
FetchContent_MakeAvailable(glfw)

FetchContent_Declare(
  glm
  GIT_REPOSITORY https://github.com/g-truc/glm.git
  GIT_TAG 0.9.9.8)
FetchContent_MakeAvailable(glm)

add_subdirectory(src)
add_subdirectory(apps)


