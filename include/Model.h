#ifndef MODEL_H
#define MODEL_H

#include <glm/glm.hpp>

#include <Mesh.h>
#include <Shader.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <string>
#include <vector>
#include <list>
#include <map>
#include <iostream>
#include <limits>

class Model{
public:
  std::vector<Texture> textures_loaded;
  std::list<Mesh> meshes;
  std::string directory;
  bool gamma_correction;
  
  Model(const std::string& path, bool gamma = false);
  
  // draws the model, and thus all its meshes
  void draw(Shader& shader);
  
  void draw_node(std::string node_name, Shader& shader, glm::mat4 model);
  
  struct Node{
    glm::mat4 transformation;
    std::vector<Mesh*> meshes;
    std::vector<Node*> children;
    
    void append_mesh(Mesh& mesh){ meshes.push_back(&mesh); }
    void append_child(Node* child){ children.push_back(child); }
    void draw(Shader& shader, glm::mat4 model, bool apply_trns=true){
      if(apply_trns) model = model * transformation;
      shader.setMat4("model", model);
      for(Mesh* m: meshes)
        m->draw(shader);
      for(Node* n: children)
        n->draw(shader, model);
    }
  };
  
  Node& get_node(std::string node_name) { return model_nodes[node_name]; }
  
private:
  std::map<std::string, Node> model_nodes;
  
  // loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
  void load_model(const std::string& path);
  
  // processes a node in a recursive fashion. 
  // Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
  Node* process_node(aiNode* node, const aiScene* scene, int x);
  Mesh process_mesh(aiMesh* mesh, const aiScene* scene);

  // checks all material textures of a given type and loads the textures if they're not loaded yet.
  // the required info is returned as a Texture struct.
  std::vector<Texture> load_material_textures(aiMaterial* mat, aiTextureType type, std::string type_name);
};

#endif
