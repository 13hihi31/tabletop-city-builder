#ifndef ENGINE_H
#define ENGINE_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include <vector>
#include <chrono>

class Engine{
public:
  void run(int video_id);
  void start_gl(float video_width, float video_height);
  void stop_gl();
  
  void framebuffer_size_callback(int width, int height);
  void mouse_callback(double xpos, double ypos);
  void scroll_callback(double xoffset, double yoffset);
  void process_input(GLFWwindow *window);
  void set_camera_front();
  
  ////

  GLFWwindow* window;
  
  //const unsigned int scr_width = 1024;
  //const unsigned int scr_height = 768;
  const unsigned int scr_width = 1920 - 100;
  const unsigned int scr_height = 1080 - 100;
  
  glm::vec3 camera_pos = glm::vec3(0.0f, 0.5f, 1.1f);
  glm::vec3 camera_front = glm::vec3(0.0f, 0.0f, -1.0f);
  glm::vec3 camera_up = glm::vec3(0.0f, 1.0f, 0.0f);

  bool first_mouse = true;
  float yaw = -90.0f;
  float pitch = -20.0f;
  float last_x = scr_width / 2.0f;
  float last_y = scr_height / 2.0f;
  float fov = 45.0f;
  const float camera_speed = 0.05f;
  
  double fps = 30.0;
  std::chrono::duration<double> frame_time = std::chrono::duration<double>(1.0 / fps);

	// vertices values are chosen to satisfy 1280x720 camera resolution for now
  std::vector<float> video_vertices;

  unsigned int video_texture;
  unsigned int vbo_video, vao_video;
};

#endif
