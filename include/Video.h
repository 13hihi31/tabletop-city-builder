#ifndef VIDEO_H
#define VIDEO_H

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>

#include <vector>
#include <thread>
#include <mutex>

class Video{
public:
  struct Marker{
    glm::vec3 center;
    float size;
    float angle;
    int id;
  };

  Video(int video_id=0);
  void run();
  unsigned int get_video_as_texture(GLenum min_filter, GLenum mag_filter, GLenum wrap_filter);
  std::vector<Marker> get_marker_data();
  bool has_new_image();
  void gen_new_image();
  void stop();
  float get_width() { return video_width; }
  float get_height() { return video_height; }
  bool is_running() { return video_is_on; }
  
  ////
  
  int video_id;
  bool continue_running = false;
  bool video_is_on = false;
  std::vector<cv::Mat> frames;
  int head, tail;
  const int buffer_size = 5;
  
  std::vector<int> marker_ids;
  std::vector<std::vector<cv::Point2f>> marker_corners, rejected_candidates;
  cv::Ptr<cv::aruco::DetectorParameters> parameters = cv::aruco::DetectorParameters::create();
  cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
  std::vector<Marker> marker_data;
  
  float video_width = 1280.0f;
  float video_height = 720.0f;
};

#endif
