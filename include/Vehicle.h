#ifndef VEHICLE_H
#define VEHICLE_H

#include <Model.h>
#include <World.h>

#include <glm/glm.hpp>

class Vehicle{
public:
  Vehicle(World& world, float x, float z, Model::Node& node);

  void run(float r);
  void draw(Shader& shader);
  void seek(glm::vec3 target);
  void apply_force(glm::vec3 force);
  void update();
  std::pair<glm::vec3, float> follow(const glm::vec3& start, const glm::vec3& end);
  
  void avoid_boarders();
  glm::vec3 wander_target(float radius);

  World world;
  Model::Node& model;
  glm::vec3 location;
  glm::vec3 velocity;
  glm::vec3 acceleration;
  float maxspeed;
  float mass;
  float maxforce;
};

#endif
