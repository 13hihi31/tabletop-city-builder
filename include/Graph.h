#ifndef GRAPH_H
#define GRAPH_H

#include <World.h>
#include <Shader.h>

#include <glm/glm.hpp>

#include <list>
#include <map>
#include <chrono>
#include <vector>

class Graph{
public:
  Graph(World& world);
  ~Graph();

  struct Node{
    Node(){}
    Node(glm::vec3 location, glm::vec4 color, int id);
    bool still_valid();
    void update(glm::vec3 new_location);
    void add_edge_to(int id);
    void remove_edge_to(int id);
    void set_color(glm::vec4 c){ color = c; }
    
    ////
    
    int id;
    glm::vec4 color;
    glm::vec3 location;
    std::list<int> adjacency_list;
    std::chrono::duration<double> expiration_time = std::chrono::duration<double>(10.0);
    std::chrono::time_point<std::chrono::steady_clock> last_time_seen;
  };

  struct Edge{
    glm::vec3 start;
    glm::vec3 end;
  };
  
  void add_node(int id, glm::vec3 location);
  void remove_node(int id);
  std::map<int, Node>::iterator remove_node(std::map<int, Node>::iterator& it);
  void highlight_node(int id, glm::vec4 color);
  void unhighlight_node(int id);
  std::pair<int, float> min_distance_node(glm::vec3 location);
  size_t get_num_nodes(){ return nodes.size(); }
  
  void add_edge(int id1, int id2);
  void remove_edge(int id1, int id2);
  std::vector<Edge> get_edges();
  
  void run();
  void draw(Shader& shader);
  
  ////
  
  World& world;
  std::map<int, Node> nodes;
  
  std::vector<float> qube_vertices;
  std::vector<float> cylinder_vertices;
  unsigned int vbo_edges, vao_edges;
  unsigned int vbo_nodes, vao_nodes;
  glm::vec4 color;
};

#endif
