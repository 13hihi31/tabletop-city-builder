#ifndef GEOMETRIC_PRIMITIVES_H
#define GEOMETRIC_PRIMITIVES_H

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include <vector>

std::vector<float> gen_qube_vertices(){
  std::vector<float> vertices = {
  -0.5f, -0.5f, -0.5f,
   0.5f, -0.5f, -0.5f,
   0.5f,  0.5f, -0.5f,
   0.5f,  0.5f, -0.5f,
  -0.5f,  0.5f, -0.5f,
  -0.5f, -0.5f, -0.5f,

  -0.5f, -0.5f,  0.5f,
   0.5f, -0.5f,  0.5f,
   0.5f,  0.5f,  0.5f,
   0.5f,  0.5f,  0.5f,
  -0.5f,  0.5f,  0.5f,
  -0.5f, -0.5f,  0.5f,

  -0.5f,  0.5f,  0.5f,
  -0.5f,  0.5f, -0.5f,
  -0.5f, -0.5f, -0.5f,
  -0.5f, -0.5f, -0.5f,
  -0.5f, -0.5f,  0.5f,
  -0.5f,  0.5f,  0.5f,

   0.5f,  0.5f,  0.5f,
   0.5f,  0.5f, -0.5f,
   0.5f, -0.5f, -0.5f,
   0.5f, -0.5f, -0.5f,
   0.5f, -0.5f,  0.5f,
   0.5f,  0.5f,  0.5f,

  -0.5f, -0.5f, -0.5f,
   0.5f, -0.5f, -0.5f,
   0.5f, -0.5f,  0.5f,
   0.5f, -0.5f,  0.5f,
  -0.5f, -0.5f,  0.5f,
  -0.5f, -0.5f, -0.5f,

  -0.5f,  0.5f, -0.5f,
   0.5f,  0.5f, -0.5f,
   0.5f,  0.5f,  0.5f,
   0.5f,  0.5f,  0.5f,
  -0.5f,  0.5f,  0.5f,
  -0.5f,  0.5f, -0.5f,
  };
  
  return vertices;
}

std::vector<float> gen_cylinder_vertices(int sides){
  float theta = 2.0f * glm::pi<float>() / static_cast<float>(sides);
  float c = glm::cos(theta);
  float s = glm::sin(theta);
  float radius = 0.5f;
  int side_triangles = 4;
  int triangle_coords = 9;
  int side_coords = side_triangles * triangle_coords;
  std::vector<float> vertices(sides * side_coords);
  float x = radius;
  float z = 0.0f;
  
  for(int i = 0; i < sides; ++i){
    int k = i * side_coords;
    float new_x = c * x - s * z;
    float new_z = s * x + c * z;
    
    // wall triangle 1
    vertices[k++] = x;
    vertices[k++] = radius;
    vertices[k++] = z;
    
    vertices[k++] = x;
    vertices[k++] = -radius;
    vertices[k++] = z;
    
    vertices[k++] = new_x;
    vertices[k++] = -radius;
    vertices[k++] = new_z;
    
    // wall triangle 2
    vertices[k++] = new_x;
    vertices[k++] = -radius;
    vertices[k++] = new_z;
    
    vertices[k++] = new_x;
    vertices[k++] = radius;
    vertices[k++] = new_z;
    
    vertices[k++] = x;
    vertices[k++] = radius;
    vertices[k++] = z;
    
    // top triangle
    vertices[k++] = x;
    vertices[k++] = radius;
    vertices[k++] = z;
    
    vertices[k++] = new_x;
    vertices[k++] = radius;
    vertices[k++] = new_z;
        
    vertices[k++] = 0.0f;
    vertices[k++] = radius;
    vertices[k++] = 0.0f;
    
    // bottom triangle
    vertices[k++] = x;
    vertices[k++] = -radius;
    vertices[k++] = z;
    
    vertices[k++] = new_x;
    vertices[k++] = -radius;
    vertices[k++] = new_z;
        
    vertices[k++] = 0.0f;
    vertices[k++] = -radius;
    vertices[k++] = 0.0f;
    
    x = new_x;
    z = new_z;
  }
  
  return vertices;
}

#endif
