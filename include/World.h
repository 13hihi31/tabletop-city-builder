#ifndef WORLD_H
#define WORLD_H

#include <glm/glm.hpp>

class World{
public:
  World(){}
  World(float video_width, float video_height){
    hw_ratio = video_height / video_width;
  }
  float get_left(){ return -0.5f; }
  float get_right(){ return 0.5f; }
  float get_bottom(){ return 0.5f * hw_ratio; }
  float get_top(){ return -0.5f * hw_ratio; }
  float get_height(){ return 0.01f; }
  glm::vec3 get_height_vec(){ return glm::vec3(0.0f, get_height(), 0.0f); }
  
  glm::vec3 get_model_scale_vec(){ return glm::vec3(model_scale_coef, model_scale_coef, model_scale_coef); }
  float get_model_scale(){ return model_scale_coef; }
  
  glm::vec3 get_scale_vec(){ return glm::vec3(scale_coef, scale_coef, scale_coef); }
  float get_scale(){ return scale_coef; }
  
  float model_scale_coef = 0.00015f;
  float scale_coef = 0.03f;
  float hw_ratio = 720.0f / 1280.0f;
};

#endif
