#ifndef ADJACENT_MATCHER_H
#define ADJACENT_MATCHER_H

#include <vector>
#include <map>
#include <chrono>

class AdjacentMatcher{
public:
  static const int not_assigned_node = -1;
  bool visited_flag = true;
  std::chrono::duration<double> match_time = std::chrono::duration<double>(2.0);
  
  struct DetectedPair{
    int node1_id = not_assigned_node;
    int node2_id = not_assigned_node;
  };
  
  struct ProspectPair{
    ProspectPair(){}
    ProspectPair(int node1_id, int node2_id, bool visited_flag):
      node1_id(node1_id),
      node2_id(node2_id),
      visited_flag(visited_flag)
    {
      detection_time = std::chrono::steady_clock::now();
    }
    
    int node1_id;
    int node2_id;
    bool visited_flag;
    std::chrono::time_point<std::chrono::steady_clock> detection_time;
  };
  
  struct MatchedPair{
    // we keep track of the marker_id to later decide what operation to execute on the pair
    int marker_id;
    int node1_id;
    int node2_id;
  };
  
  std::map<int, ProspectPair> prospect_pairs;
  std::map<int, DetectedPair> detected_pairs;
  
  bool match(const ProspectPair& p){
    auto time_passed_since_detection = std::chrono::steady_clock::now() - p.detection_time;
    return time_passed_since_detection > match_time;
  }
  
  void update_adjacent(int marker_id, int node_id){
    std::map<int, DetectedPair>::iterator it = detected_pairs.find(marker_id);
    if(it != detected_pairs.end()){
      it->second.node2_id = node_id;
    }
    else{
      detected_pairs[marker_id] = {.node1_id=node_id};
      //detected_pairs.emplace(marker_id, {.node1_id=node_id});
    }
  }
  
  std::vector<MatchedPair> get_matched_pairs(){
    std::vector<MatchedPair> matched_pairs;
    visited_flag = !visited_flag;
    
    for(auto& p: detected_pairs){
      if(p.second.node2_id != not_assigned_node && p.second.node1_id != p.second.node2_id){
        std::map<int, ProspectPair>::iterator it = prospect_pairs.find(p.first);
        if(it != prospect_pairs.end()){
          if(match(it->second)){
            matched_pairs.push_back({.marker_id=p.first, .node1_id=p.second.node1_id, .node2_id=p.second.node2_id});
            prospect_pairs.erase(it);
          }
          else{
            it->second.visited_flag = visited_flag;
          }
        }
        else{
          //prospect_pairs[p.first] = ProspectPair(p.second.node1_id, p.second.node2_id, visited_flag);
          prospect_pairs.emplace(p.first, ProspectPair(p.second.node1_id, p.second.node2_id, visited_flag));
        }
      }
    }
    
    // clean those pairs that where not visited i.e. have an outdated visited_flag value
    for(auto it = prospect_pairs.begin(); it != prospect_pairs.end(); ){
      if(it->second.visited_flag != visited_flag){
        it = prospect_pairs.erase(it);
      }
      else{
        ++it;
      }
    }
    
    detected_pairs.clear();
    
    return matched_pairs;
  }
};

#endif
