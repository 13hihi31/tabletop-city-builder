#version 330 core

out vec4 FragColor;
in vec2 TexCoord;

uniform sampler2D camera_texture;

void main(){
  FragColor = texture(camera_texture, TexCoord);
}

