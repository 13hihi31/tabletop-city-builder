
#include <Engine.h>

#include <iostream>
#include <string>
#include <unistd.h>
    
int main(int argc, char** argv){
  std::cout << "process pid: " << getpid() << std::endl;
  
  int video_id = 0;
  if(argc == 2){
    video_id = std::stoi(argv[1]);
  }
  
  Engine engine;
  engine.run(video_id);
}

