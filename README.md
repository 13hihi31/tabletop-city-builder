<table align="center"><tr><td align="center" width="9999">
<img src="media/webcam_top_orientation.jpg" align="center" width="900" alt="Picture of the system working">

# Tabletop City Builder

a tabletop city builder augmented reality application prototype with inherent multiuser/multiplayer mode
</td></tr></table>

# Build Setup
The requirements are:

* CMake 3.14 or better
* A C++17 compatible compiler
* OpenGl 3.3
* Glew 2.1
* OpenCv 4.2 or better
* Git
* Assimp

On Ubuntu Linux the required packages can be installed with the following command:
```bash
sudo apt install build-essential cmake git libopencv-dev libglu1-mesa-dev libglew-dev glew-utils libxinerama-dev libxrandr-dev libxcursor-dev libxi-dev libassimp-dev assimp-utils
```
(Note: dificulties where encountered in linking the OpenCv library, if you were to encounter similar dificulties the place that may need adjustments is the file /src/CMakeLists.txt)

To configure and build:

```bash
./configure_and_build_app
```

To run the app (with webcam under the device number set to 0):

```bash
./run
```

To run the app with webcam selection:

```bash
./run webcam-device-number
```

example:

```bash
./run 1
```

# Usage
* While the app is running, use WASD keys and mouse to move and orient the camera, press ESC to exit the app.
* Print the file aruco_markers_set1.pdf and cut out the Aruco markers using scissors or what have you.
* Use special operation markers like 'connect' to connect node markers with a road.
* Use special operation markers like 'disconnect' to disconnect node markers that are connected with a road.
* Place markers named for instance: 'building #' to place a building on the map.
* Occluding or removing a marker from the field of view disconnects the connected roads to that marker after occlusion timer hits timeout.

<table align="center">
  <tr>
    <td align="center" width="9999">
    <figure>
    <img src="media/connect.gif" align="center" width="900" alt="A Gif image showing the steps needed to connect a pair of nodes">
    <figcaption> Connecting nodes with a road </figcaption>
    </figure>
    </td>
  </tr>
  <tr>
    <td align="center" width="9999">
    <figure>
    <img src="media/disconnect.gif" align="center" width="900" alt="A Gif image showing the steps needed to disconnect a pair of nodes">
    <figcaption> Disconnecting nodes </figcaption>
    </figure>
    </td>
  </tr>
  <tr>
    <td align="center" width="9999">
    <figure>
    <img src="media/add_building.gif" align="center" width="900" alt="A Gif image showing the steps needed to place a new building on the map">
    <figcaption> Adding a building </figcaption>
    </figure>
    </td>
  </tr>
  <tr>
    <td align="center" width="9999">
    <figure>
    <img src="media/remove_marker.gif" align="center" width="900" alt="A Gif image showing what happens when a connected node marker is removed from the webcam field of view">
    <figcaption> Removing a marker from the webcam field of view </figcaption>
    </figure>
    </td>
  </tr>
</table>

Markers that have a name like 'connect', 'disconnect', 'building 1' can be seen by the webcam in any number. In particular more than one person can simultaneously connect different markers with roads. The system is inherently multiplayer with no discrimination with respect to who is sitting at the computer. Markers that do not have a name are meant to be unique, do not place on the table two markers with the same visual code. This is only relevant if more than one pair of 'connect'/'disconnect' marker pairs are needed and the file aruco_markers_set1.pdf will be printed more than once, in which case discard all the extra copies of markers that do not have a name.

For best immersion orient the webcam perpendicular to the table surface. Webcam orientation at an angle to the table surface also works but is less aesthetically appealing.

<table align="center">
  <tr>
    <td align="center" width="9999">
    <figure>
    <img src="media/webcam_tilted_orientation.jpg" align="center" width="900" alt="An image showing how the system works when the webcam is oriented at an angle to the table surface">
    <figcaption> Setup with webcam oriented at an angle to the table surface </figcaption>
    </figure>
    </td>
  </tr>
</table>

# Project Vision
<table align="center">
  <tr>
    <td align="center" width="9999">
    <figure>
    <img src="media/initial_vision.jpg" align="center" width="900" alt="An image of a drawing of the initial vision of the system working">
    <figcaption> A drawing of the initial system vision </figcaption>
    </figure>
    </td>
  </tr>
</table>

The primary source of inspiration for this project is the [Dynamicland system](https://dynamicland.org). While this project does not realize any of the functions the Dynamicland offers it was an attempt to better understand the constraints of designing a system where the input is primarily visual and the user does not respond to the available options presented by the program but instead the program responds to the unconstrained actions of the user. The city building aspect of this project was not crucial but served as a perfect test bead of a dynamic system that had to respond to user actions.

This project is considered finished, although substantial refactoring is still needed to bring it to a good shape. Instead of refactoring, a new version of the system with a bigger scope is being designed from ground up, probably using an ECS oriented architecture.
 
# Credits
* The OpenGl knowledge needed for this project comes from the excellent [online e-book](https://learnopengl.com) by [Joey De Vries](https://twitter.com/JoeyDeVriez). The classes: [Shader](https://learnopengl.com/code_viewer_gh.php?code=includes/learnopengl/shader.h), [Mesh](https://learnopengl.com/code_viewer_gh.php?code=includes/learnopengl/mesh.h) and [Model](https://learnopengl.com/code_viewer_gh.php?code=includes/learnopengl/model.h) used in this project have been used from the book with or without modification. For example the class Model was modified to render only selected parts of the 3d model. The code used from the [Joey De Vries book](https://learnopengl.com) is under [CC BY-NC 4.0 license](https://creativecommons.org/licenses/by-nc/4.0/).
* The used [3d model](https://sketchfab.com/3d-models/cartoon-lowpoly-small-city-free-pack-edd1c604e1e045a0a2a552ddd9a293e6) under the [CC Attribution license](https://creativecommons.org/licenses/by/4.0/) was made by the 3d artist [Anton Moek](https://antonmoek.com/projects).
