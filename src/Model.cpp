
#include <Model.h>
#include <Mesh.h>
#include <Shader.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>

namespace {

  unsigned int texture_from_file(const char* path, const std::string& directory, bool gamma = false){
    std::string filename = std::string(path);
    filename = directory + '/' + filename;
    
    unsigned int texture_id;
    glGenTextures(1, &texture_id);

    int width, height, nr_components;
    unsigned char* data = stbi_load(filename.c_str(), &width, &height, &nr_components, 0);
    if(data){
      GLenum format;
      if(nr_components == 1)
        format = GL_RED;
      else if(nr_components == 3)
        format = GL_RGB;
      else if(nr_components == 4)
        format = GL_RGBA;

      glBindTexture(GL_TEXTURE_2D, texture_id);
      glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
      glGenerateMipmap(GL_TEXTURE_2D);

      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

      stbi_image_free(data);
    }
    else{
      std::cout << "Texture failed to load at path: " << path << std::endl;
      stbi_image_free(data);
    }

    return texture_id;
  }

  glm::mat4 aiMatrix4x4_to_mat4(const aiMatrix4x4& m){
    float matrix[16] = 
    {m.a1, m.a2, m.a3, m.a4,
     m.b1, m.b2, m.b3, m.b4,
     m.c1, m.c2, m.c3, m.c4,
     m.d1, m.d2, m.d3, m.d4};
    /*
    for(auto i : matrix)
      std::cout << i << " ";
    std::cout << "\n";
    */
    return glm::transpose(glm::make_mat4x4(matrix));
  }

};

Model::Model(const std::string& path, bool gamma):
  gamma_correction(gamma)
{
  load_model(path);
}

// draws the model, and thus all its meshes
void Model::draw(Shader& shader){
  for(Mesh& m : meshes){
    m.draw(shader);
  }
}

void Model::draw_node(std::string node_name, Shader& shader, glm::mat4 model){
  auto node_it = model_nodes.find(node_name);
  if(node_it != model_nodes.end()){
    node_it->second.draw(shader, model);
  }
}

// loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
void Model::load_model(const std::string& path){
  // read file via ASSIMP
  Assimp::Importer importer;
  const aiScene* scene = importer.ReadFile(std::string(path), 
    aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
  // check for errors
  if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode){
      std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
      return;
  }
  // retrieve the directory path of the filepath
  directory = path.substr(0, path.find_last_of('/'));

  // process ASSIMP's root node recursively
  process_node(scene->mRootNode, scene, 0);
}

// processes a node in a recursive fashion. 
// Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
Model::Node* Model::process_node(aiNode* node, const aiScene* scene, int depth){
  ///
  // print the model tree
  //for(int i = 0; i < depth; ++i){ std::cout << " "; }
  //std::cout << node->mName.C_Str() << " " << node->mNumMeshes << std::endl;
  
  std::string node_name = node->mName.C_Str();
  model_nodes[node_name] = Model::Node();
  Model::Node& model_node = model_nodes[node_name]; 
  model_node.transformation = aiMatrix4x4_to_mat4(node->mTransformation);
  ///
  
  // process each mesh located at the current node
  for(unsigned int i = 0; i < node->mNumMeshes; ++i){
    // the node object only contains indices to index the actual objects in the scene. 
    // the scene contains all the data, node is just to keep stuff organized (like relations between nodes).
    aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
    meshes.push_back(process_mesh(mesh, scene));
    model_node.append_mesh(meshes.back()); ///
  }
  // after we've processed all of the meshes (if any) we then recursively process each of the children nodes
  for(unsigned int i = 0; i < node->mNumChildren; i++){
    model_node.append_child(process_node(node->mChildren[i], scene, depth + 1)); ///
  }
  
  return &model_node; ///
}

Mesh Model::process_mesh(aiMesh* mesh, const aiScene* scene){
  // data to fill
  std::vector<Vertex> vertices;
  std::vector<unsigned int> indices;
  std::vector<Texture> textures;

  // walk through each of the mesh's vertices
  for(unsigned int i = 0; i < mesh->mNumVertices; ++i){
    Vertex vertex;
    // we declare a placeholder vector since assimp uses its own vector class that doesn't directly
    // convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
    glm::vec3 vector;
    // positions
    vector.x = mesh->mVertices[i].x;
    vector.y = mesh->mVertices[i].y;
    vector.z = mesh->mVertices[i].z;
    vertex.position = vector;
    // normals
    if(mesh->HasNormals()){
      vector.x = mesh->mNormals[i].x;
      vector.y = mesh->mNormals[i].y;
      vector.z = mesh->mNormals[i].z;
      vertex.normal = vector;
    }
    // texture coordinates
    // does the mesh contain texture coordinates?
    if(mesh->mTextureCoords[0]){
      glm::vec2 vec;
      // a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
      // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
      vec.x = mesh->mTextureCoords[0][i].x; 
      vec.y = mesh->mTextureCoords[0][i].y;
      vertex.tex_coords = vec;
      // tangent
      vector.x = mesh->mTangents[i].x;
      vector.y = mesh->mTangents[i].y;
      vector.z = mesh->mTangents[i].z;
      vertex.tangent = vector;
      // bitangent
      vector.x = mesh->mBitangents[i].x;
      vector.y = mesh->mBitangents[i].y;
      vector.z = mesh->mBitangents[i].z;
      vertex.bitangent = vector;
    }
    else{
      vertex.tex_coords = glm::vec2(0.0f, 0.0f);
    }

    vertices.push_back(vertex);
  }
  // now wak through each of the mesh's faces (a face is a mesh its triangle)
  // and retrieve the corresponding vertex indices.
  for(unsigned int i = 0; i < mesh->mNumFaces; ++i){
    aiFace face = mesh->mFaces[i];
    // retrieve all indices of the face and store them in the indices vector
    for(unsigned int j = 0; j < face.mNumIndices; ++j){
      indices.push_back(face.mIndices[j]);
    }
  }
  // process materials
  aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];    
  // we assume a convention for sampler names in the shaders. Each diffuse texture should be named
  // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
  // Same applies to other texture as the following list summarizes:
  // diffuse: texture_diffuseN
  // specular: texture_specularN
  // normal: texture_normalN

  // 1. diffuse maps
  std::vector<Texture> diffuse_maps = load_material_textures(material, aiTextureType_DIFFUSE, "texture_diffuse");
  textures.insert(textures.end(), diffuse_maps.begin(), diffuse_maps.end());
  // 2. specular maps
  std::vector<Texture> specular_maps = load_material_textures(material, aiTextureType_SPECULAR, "texture_specular");
  textures.insert(textures.end(), specular_maps.begin(), specular_maps.end());
  // 3. normal maps
  std::vector<Texture> normal_maps = load_material_textures(material, aiTextureType_HEIGHT, "texture_normal");
  textures.insert(textures.end(), normal_maps.begin(), normal_maps.end());
  // 4. height maps
  std::vector<Texture> height_maps = load_material_textures(material, aiTextureType_AMBIENT, "texture_height");
  textures.insert(textures.end(), height_maps.begin(), height_maps.end());
  
  // return a mesh object created from the extracted mesh data
  return Mesh(vertices, indices, textures);
}

// checks all material textures of a given type and loads the textures if they're not loaded yet.
// the required info is returned as a Texture struct.
std::vector<Texture> Model::load_material_textures(aiMaterial* mat, aiTextureType type, std::string type_name){
  std::vector<Texture> textures;
  for(unsigned int i = 0; i < mat->GetTextureCount(type); ++i){
    aiString str;
    mat->GetTexture(type, i, &str);
    // check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
    bool skip = false;
    for(unsigned int j = 0; j < textures_loaded.size(); ++j){
      if(std::strcmp(textures_loaded[j].path.data(), str.C_Str()) == 0){
        textures.push_back(textures_loaded[j]);
        // a texture with the same filepath has already been loaded, continue to next one. (optimization)
        skip = true;
        break;
      }
    }
    if(!skip){
      // if texture hasn't been loaded already, load it
      Texture texture;
      texture.id = texture_from_file(str.C_Str(), this->directory);
      texture.type = type_name;
      texture.path = str.C_Str();
      textures.push_back(texture);
      // store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
      textures_loaded.push_back(texture);
    }
  }
  return textures;
}

