
#include <Vehicle.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/random.hpp>

Vehicle::Vehicle(World& world, float x, float z, Model::Node& model):
  world(world),
  model(model),
  location(glm::vec3(x, 0.0f, z)),
  velocity(glm::vec3(1.0f, 0.0f, 0.0f)),
  acceleration(glm::vec3(0.0f, 0.0f, 0.0f)),
  mass(1.0f)
{
  float width = std::abs(world.get_left()) + std::abs(world.get_right());
  maxspeed = width / 700.0f;
  maxforce = width / 3000.0f;
  float std = maxspeed / 4.0f;
  velocity = glm::sphericalRand(maxspeed) + 
  glm::gaussRand(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(std, std, std));
  velocity.y = 0.0f;
}

std::pair<glm::vec3, float> Vehicle::follow(const glm::vec3& start, const glm::vec3& end){
  glm::vec3 predicted = velocity;
  predicted *= 5.0f;
  predicted += location;

  // get the normal point of the predicted future location on the start-end edge 
  glm::vec3 a = predicted - start;
  glm::vec3 endstart = end - start;
  glm::vec3 unit_endstart = glm::normalize(endstart);
  float dot_prod = glm::dot(a, unit_endstart);
  glm::vec3 normal_point = start + unit_endstart * dot_prod;

  if(dot_prod < 0.0f || dot_prod > glm::length(endstart)){
    normal_point = start;
  }
  
  glm::vec3 target = normal_point + unit_endstart * 10.0f * maxspeed;
  
  /*
  float penalty = 1.0f;
  if(glm::length(location - end) < world.get_scale() * 1.5f){
    penalty = 2.0f;
  }
  */
  
  //return std::make_pair(target, penalty * glm::length(predicted - normal_point));
  return std::make_pair(target, glm::length(predicted - normal_point));
}

void Vehicle::avoid_boarders(){
  if(location.x < world.get_left()){
	  glm::vec3 steer = glm::vec3(velocity.x + maxspeed, 0.0f, 0.0f);
	  steer = glm::clamp(steer, -maxforce, maxforce);
	  apply_force(steer);
  }
  else if(location.x > world.get_right()){
    glm::vec3 steer = glm::vec3(velocity.x - maxspeed, 0.0f, 0.0f);
	  steer = glm::clamp(steer, -maxforce, maxforce);
	  apply_force(steer);
  }

  if(location.z > world.get_bottom()){
    glm::vec3 steer = glm::vec3(0.0f, 0.0f, velocity.z - maxspeed);
	  steer = glm::clamp(steer, -maxforce, maxforce);
	  apply_force(steer);
  }
  else if(location.z < world.get_top()){
    glm::vec3 steer = glm::vec3(0.0f, 0.0f, velocity.z + maxspeed);
	  steer = glm::clamp(steer, -maxforce, maxforce);
	  apply_force(steer);
  }
}

glm::vec3 Vehicle::wander_target(float radius){
  glm::vec3 change = glm::sphericalRand(radius);
  if(0.9 < glm::linearRand(0.0f, 1.0f))
    change *= 10.0f;
    
  glm::vec3 direction = 3.0f * world.get_scale() * glm::normalize(velocity);
  glm::vec3 next_location = location + direction + change;
  next_location.y = 0.0f;
  return next_location;
}

void Vehicle::seek(glm::vec3 target){
  glm::vec3 desired = target - location;
  desired = glm::normalize(desired);
  desired *= maxspeed;
  glm::vec3 steer = desired - velocity;
  steer = glm::clamp(steer, -maxforce, maxforce);
  apply_force(steer);
}

void Vehicle::apply_force(glm::vec3 force){
  force /= mass;
  acceleration += force;
}

void Vehicle::update(){
  velocity += acceleration;
  velocity = glm::clamp(velocity, -maxspeed, maxspeed);
  location += velocity;
  acceleration *= 0.0f;
}

void Vehicle::run(float r){
  glm::vec3 next_lock = wander_target(r);
  seek(next_lock);
  avoid_boarders();
  update();
}

void Vehicle::draw(Shader& shader){
  glm::mat4 model_mat = glm::mat4(1.0f);
  model_mat = glm::translate(model_mat, location + world.get_height_vec());
  float angle = glm::atan(-velocity.z, velocity.x);
  model_mat = glm::rotate(model_mat, angle - glm::half_pi<float>(), glm::vec3(0.0f, 1.0f, 0.0f));
  model_mat = glm::scale(model_mat, world.get_model_scale_vec());
  model.draw(shader, model_mat, false);
}

