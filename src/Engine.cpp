
#include <Engine.h>
#include <Model.h>
#include <Shader.h>
#include <Video.h>
#include <Vehicle.h>
#include <World.h>
#include <Graph.h>
#include <AdjacentMatcher.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/random.hpp>

#include <iostream>
#include <chrono>
#include <thread>
#include <cmath>
#include <cstdlib>
#include <exception>
#include <vector>
#include <string>

void Engine::run(int video_id){
  Video video(video_id);
  std::thread video_thread(&Video::run, &video);
  
  // wait for the camera to switch on
  // so that we know what the video frame shape
  // TODO: add exception handling and more descriptive problem source
  auto timeout = std::chrono::duration<double>(10.);
  auto start = std::chrono::steady_clock::now();
  while(!video.is_running()){
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    auto now = std::chrono::steady_clock::now();
    if(now - start > timeout){
      video_thread.join();
      return;
    }
  }
  
  ////
   
  start_gl(video.get_width(), video.get_height());
  
  set_camera_front();
  
  // build and compile our shader programs
  Shader video_shader("./shaders/video_texture.glslv", "./shaders/video_texture.glslf");
  Shader model_shader("./shaders/model_texture.glslv", "./shaders/model_texture.glslf");
  Shader graph_shader("./shaders/roads_texture.glslv", "./shaders/roads_texture.glslf");

  // load model
  Model city_model("./assets/cartoon-lowpoly-small-city-free-pack/Small_City_Free_Pack.fbx");
  
  std::vector<std::string> car_names = {
    "CAR_03_1",
    "CAR_03",
    "Car_04",
    "CAR_03_2",
    "Car_04_1",
    "Car_04_2",
    "Car_04_3",
    "Car_04_4",
    //"Car_08_4",
    //"Car_08_3",
    "Car_04_1_2",
    //"Car_08_2",
    "CAR_03_1_2",
    "CAR_03_2_2",
    "Car_04_2_2",
    //"Car_08_1",
    //"Car_08",
    //"Car_08_5",
    "CAR_03_2_3",
    //"Car_08_6",
    "CAR_03_3"
  };

  std::vector<std::string> block_names = {
    "Block_1",
    "Block_2",
    "Block_3",
    "Block_4"
  };
  
  World world(video.get_width(), video.get_height());
  
  // create vehicles
  // TODO: the vehicles should be spawned from buildings
  //       extend the vehicle with more agency or create a new type Agent that could be driving the vehicle
  
  std::vector<Vehicle> vehicles;
  for(size_t i = 0; i < 100; ++i){
    float x = glm::linearRand(world.get_left(), world.get_right());
    float z = glm::linearRand(world.get_top(), world.get_bottom());
    vehicles.push_back(Vehicle(world, x, z, city_model.get_node(car_names[i % car_names.size()])));
  }

  Graph graph(world);
  AdjacentMatcher adjacent_matcher;
  
  unsigned int video_texture = 0;
  std::vector<Video::Marker> markers;
  
  ////
  
  while(!glfwWindowShouldClose(window)){
    auto start = std::chrono::steady_clock::now();
    
    process_input(window);
    
    // render
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    ////
    
    // for now the only way to control the camera position is through ASWD keys and mouse
    // TODO: add option to atach the camera above one of the driving vehicles in a third person perspective fashion
    
    glm::mat4 model = glm::mat4(1.0f);
		
    glm::mat4 view;
    view = glm::lookAt(camera_pos, camera_pos + camera_front, camera_up);
    
    glm::mat4 projection = glm::mat4(1.0f);
    projection = glm::perspective(glm::radians(fov), static_cast<float>(scr_width) / static_cast<float>(scr_height), 0.1f, 100.0f);
    
    ////

    // draw video image on verticies

    if(video.has_new_image()){
      glDeleteTextures(1, &video_texture);
      video_texture = video.get_video_as_texture(GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, GL_CLAMP);
      markers = video.get_marker_data();
      video.gen_new_image();
    }
    
    // bind textures on corresponding texture units
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, video_texture);
    
    video_shader.use();
    video_shader.setInt("video_texture", 0);
    video_shader.setMat4("projection", projection);
    video_shader.setMat4("view", view);
    video_shader.setMat4("model", model);
    
    // draw the camera frame
    glBindVertexArray(vao_video);
    glDrawArrays(GL_TRIANGLES, 0, video_vertices.size());
    glBindVertexArray(0);
    
    ////

    // draw buildings on appropriate markers
    // for now since the buildings are non active objects the model is simply drawn over the marker
    // TODO: this will be changed to a factory generating different building objects for appropriate markers
    //       just like for nodes when the marker will disappear the building will have expiration date

    model_shader.use();
    model_shader.setMat4("projection", projection);
    model_shader.setMat4("view", view);
		
    for(size_t i = 0; i < markers.size(); ++i){
      Video::Marker marker = markers[i];
      if(marker.id < block_names.size()){
        model = glm::mat4(1.0f);
        model = glm::translate(model, marker.center);
        model = glm::translate(model, glm::vec3(0.0f, world.get_scale(), 0.0f));
        model = glm::rotate(model, marker.angle, glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::scale(model, world.get_model_scale_vec());
        Model::Node block = city_model.get_node(block_names[marker.id]);
        block.draw(model_shader, model, false);
      }
    }
    
    ////
    
    // update and draw the vehicles
    // for now the vehicle follows the closest road to it
    // TODO: add task to vehicles and destination nodes
    //       use A* to search appropriate edges to follow
    //       this is non trivial since the vehicle can be offroad or anywhere in between the roads ends
    
    auto edges = graph.get_edges();
	  
    for(size_t i = 0; i < vehicles.size(); ++i){
      Vehicle& v = vehicles[i];
      
      if(!edges.empty()){
        glm::vec3 closest_edge_target;
        float closest_edge_dist = std::numeric_limits<float>::max();
        for(auto& edge: edges){
          std::pair<glm::vec3, float> p = v.follow(edge.start, edge.end);
          if(p.second < closest_edge_dist){
            closest_edge_target = p.first;
            closest_edge_dist = p.second;
          }
        }
        
        v.seek(closest_edge_target);
      }
      
      v.run(world.get_scale() * 0.25f);
      v.draw(model_shader);
    }
		
    ////

    // detect special markers used for operations by the user and update the graph
    // TODO: here chain of responsibility pattern could be used
    //       define more operations like ataching a building to a graph node for example
    //       or recovering from history lost connections between nodes due to marker oclusion

    for(auto& marker: markers){
      if(marker.id > 5){
        graph.add_node(marker.id, marker.center);
      }
      else if(marker.id == 4){
        std::pair<int, float> adjacent = graph.min_distance_node(marker.center);
        if(adjacent.first != -1 && adjacent.second < marker.size * 2.0f){
          graph.highlight_node(adjacent.first, glm::vec4(0.6f, 0.9f, 0.0f, 1.0f));
          adjacent_matcher.update_adjacent(marker.id, adjacent.first);
        }
      }
      else if(marker.id == 5){
        std::pair<int, float> adjacent = graph.min_distance_node(marker.center);
        if(adjacent.first != -1 && adjacent.second < marker.size * 2.0f){
          graph.highlight_node(adjacent.first, glm::vec4(1.0f, 0.5f, 0.2f, 1.0f));
          adjacent_matcher.update_adjacent(marker.id, adjacent.first);
        }
      }
    }

    for(auto& p: adjacent_matcher.get_matched_pairs()){
      if(p.marker_id == 4){
        graph.add_edge(p.node1_id, p.node2_id);
      }
      else if(p.marker_id == 5){
        graph.remove_edge(p.node1_id, p.node2_id);
      }
    }

    graph_shader.use();
    graph_shader.setMat4("projection", projection);
    graph_shader.setMat4("view", view);
		
    graph.run();
    graph.draw(graph_shader);
		
    ////
		
    // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
    glfwSwapBuffers(window);
    glfwPollEvents();
    
    // delay the game loop to get a constant frame rate
    auto elapsed_time = std::chrono::steady_clock::now() - start;
    if(elapsed_time < frame_time){
      auto sleep_time = frame_time - elapsed_time;
      std::this_thread::sleep_for(sleep_time);
    }
  }
  
  stop_gl();
  
  video.stop();
  video_thread.join();
}

void Engine::start_gl(float video_width, float video_height){
  // Initialise GLFW
  if(!glfwInit()){
    throw std::runtime_error("Failed to initialize GLFW");
  }

  glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL

  // Open a window and create its OpenGL context
  window = glfwCreateWindow(scr_width, scr_height, "Tabletop City Builder", NULL, NULL);
  if(window == NULL){
    glfwTerminate();
    throw std::runtime_error("Failed to open GLFW window");
  }
  
  glfwMakeContextCurrent(window);
  
  ////
  
  glfwSetWindowUserPointer(window, this);
  
  auto cf1 = [](GLFWwindow* window, int width, int height){
    static_cast<Engine*>(glfwGetWindowUserPointer(window))->framebuffer_size_callback(width, height);
  };
  glfwSetFramebufferSizeCallback(window, cf1);
  
  auto cf2 = [](GLFWwindow* window, double xpos, double ypos){
    static_cast<Engine*>(glfwGetWindowUserPointer(window))->mouse_callback(xpos, ypos);
  };
  glfwSetCursorPosCallback(window, cf2);
  
  auto cf3 = [](GLFWwindow* window, double xoffset, double yoffset){
    static_cast<Engine*>(glfwGetWindowUserPointer(window))->scroll_callback(xoffset, yoffset);
  };
  glfwSetScrollCallback(window, cf3);
  
  ////
  
  // tell GLFW to capture our mouse
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  
  // Initialize GLEW
  glewExperimental = true; // Needed in core profile
  if(glewInit() != GLEW_OK){
    glfwTerminate();
    throw std::runtime_error("Failed to initialize GLEW");
  }
  
  glEnable(GL_DEPTH_TEST);
  
  ////
  
  float hw_ratio = video_height / video_width;
  video_vertices = {
    -0.5f, 0.0f, -0.5f * hw_ratio, 1.0f, 1.0f,
     0.5f, 0.0f, -0.5f * hw_ratio, 0.0f, 1.0f,
     0.5f, 0.0f,  0.5f * hw_ratio, 0.0f, 0.0f,
     0.5f, 0.0f,  0.5f * hw_ratio, 0.0f, 0.0f,
    -0.5f, 0.0f,  0.5f * hw_ratio, 1.0f, 0.0f,
    -0.5f, 0.0f, -0.5f * hw_ratio, 1.0f, 1.0f,
  };
  
  // bind array and buffer
  glGenVertexArrays(1, &vao_video);
  glGenBuffers(1, &vbo_video);
  glBindVertexArray(vao_video);

  glBindBuffer(GL_ARRAY_BUFFER, vbo_video);
  glBufferData(GL_ARRAY_BUFFER, video_vertices.size() * sizeof(float), &video_vertices[0], GL_STATIC_DRAW);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  
  // texture coord attirbute
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);
}
  
void Engine::stop_gl(){
  glDeleteVertexArrays(1, &vao_video);
  glDeleteBuffers(1, &vbo_video);
  
  // Close OpenGL window and terminate GLFW
  glfwTerminate();
}

void Engine::framebuffer_size_callback(int width, int height){
  // glfw: whenever the window size changed (by OS or user resize) this callback function executes
  // make sure the viewport matches the new window dimensions; note that width and 
  // height will be significantly larger than specified on retina displays
  glViewport(0, 0, width, height);
}

void Engine::mouse_callback(double xpos, double ypos){
  if(first_mouse){
    last_x = xpos;
    last_y = ypos;
    first_mouse = false;
  }
  
  float xoffset = xpos - last_x;
  float yoffset = last_y - ypos;
  last_x = xpos;
  last_y = ypos;
  
  float sensitivity = 0.1f;
  xoffset *= sensitivity;
  yoffset *= sensitivity;
  
  yaw += xoffset;
  pitch += yoffset;
  
  if(pitch > 89.0f)
    pitch = 89.0f;
  if(pitch < -89.0f)
    pitch = -89.0f;
  
  set_camera_front();
}

void Engine::set_camera_front(){
  glm::vec3 direction;
  direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
  direction.y = sin(glm::radians(pitch));
  direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
  camera_front = glm::normalize(direction);
}

void Engine::scroll_callback(double xoffset, double yoffset){
  fov -= static_cast<float>(yoffset);
  if(fov < 1.0f)
    fov = 1.0f;
  if(fov > 45.0f)
    fov = 45.0f;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
void Engine::process_input(GLFWwindow* window){
  if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);
  if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    camera_pos += camera_speed * camera_front;
  if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    camera_pos -= camera_speed * camera_front;
  if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    camera_pos -= glm::normalize(glm::cross(camera_front, camera_up)) * camera_speed;
  if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    camera_pos += glm::normalize(glm::cross(camera_front, camera_up)) * camera_speed;
}

