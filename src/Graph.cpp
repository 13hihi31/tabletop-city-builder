
#include <Graph.h>
#include <GeometricPrimitives.h>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <algorithm>
#include <limits>
#include <utility>

////

Graph::Node::Node(glm::vec3 location, glm::vec4 color, int id):
  location(location),
  color(color),
  id(id)
{
  last_time_seen = std::chrono::steady_clock::now();
}
    
bool Graph::Node::still_valid(){
  auto time_passed_since_seen = std::chrono::steady_clock::now() - last_time_seen;
  return time_passed_since_seen < expiration_time;
}

void Graph::Node::update(glm::vec3 new_location){
  location = new_location;
  last_time_seen = std::chrono::steady_clock::now();
}

void Graph::Node::add_edge_to(int id){
  adjacency_list.insert(std::lower_bound(adjacency_list.begin(), adjacency_list.end(), id), id);
}

void Graph::Node::remove_edge_to(int id){
  adjacency_list.remove(id);
}

////

Graph::Graph(World& world):
  world(world)
{
  qube_vertices = gen_qube_vertices();
  cylinder_vertices = gen_cylinder_vertices(15);
  
  // edges
  glGenVertexArrays(1, &vao_edges);
  glGenBuffers(1, &vbo_edges);
  glBindVertexArray(vao_edges);

  glBindBuffer(GL_ARRAY_BUFFER, vbo_edges);
  glBufferData(GL_ARRAY_BUFFER, qube_vertices.size() * sizeof(float), &qube_vertices[0], GL_STATIC_DRAW);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  
  // nodes
  glGenVertexArrays(1, &vao_nodes);
  glGenBuffers(1, &vbo_nodes);
  glBindVertexArray(vao_nodes);
  
  glBindBuffer(GL_ARRAY_BUFFER, vbo_nodes);
  glBufferData(GL_ARRAY_BUFFER, cylinder_vertices.size() * sizeof(float), &cylinder_vertices[0], GL_STATIC_DRAW);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
  glEnableVertexAttribArray(0);
  
  color = glm::vec4(0.38f, 0.38f, 0.38f, 1.0f);
}

Graph::~Graph(){
  glDeleteVertexArrays(1, &vao_edges);
  glDeleteBuffers(1, &vbo_edges);
  
  glDeleteVertexArrays(1, &vao_nodes);
  glDeleteBuffers(1, &vbo_nodes);
}

void Graph::add_node(int id, glm::vec3 location){
  std::map<int, Graph::Node>::iterator it = nodes.find(id);
  if(it != nodes.end()){
    it->second.update(location);
  }
  else{
    nodes[id] = Graph::Node(location, color, id);
  }
}

void Graph::remove_node(int id){
  std::map<int, Graph::Node>::iterator it = nodes.find(id);
  if(it != nodes.end()){
    remove_node(it);
  }
}

std::map<int, Graph::Node>::iterator Graph::remove_node(std::map<int, Graph::Node>::iterator& it){
  int id = it->first;
  // before deletion of the node make sure all nodes connected to it are disconnected first
  for(auto i: it->second.adjacency_list){
    nodes[i].remove_edge_to(id);
  }
  return nodes.erase(it);
}

void Graph::highlight_node(int id, glm::vec4 color){
  std::map<int, Graph::Node>::iterator it = nodes.find(id);
  if(it != nodes.end()){
    it->second.set_color(color);
  }
}

void Graph::unhighlight_node(int id){
  std::map<int, Graph::Node>::iterator it = nodes.find(id);
  if(it != nodes.end()){
    it->second.set_color(color);
  }
}

std::pair<int, float> Graph::min_distance_node(glm::vec3 location){
  int id = -1;
  float min_distance = std::numeric_limits<float>::max();
  for(auto& node : nodes){
    float d = glm::distance(location, node.second.location);
    if(d < min_distance){
      min_distance = d;
      id = node.first;
    }
  }
  return std::make_pair(id, min_distance);
}

void Graph::add_edge(int id1, int id2){
  std::map<int, Graph::Node>::iterator it1 = nodes.find(id1);
  std::map<int, Graph::Node>::iterator it2 = nodes.find(id2);
  if(it1 != nodes.end() && it2 != nodes.end()){
    std::list<int>& a_list = nodes[id1].adjacency_list;
    std::list<int>::iterator it = std::find(a_list.begin(), a_list.end(), id2);
    // avoid putting the same edge twice in to the adjacency lists
    if(it == a_list.end()){
      nodes[id1].add_edge_to(id2);
      nodes[id2].add_edge_to(id1);
    }
  }
}

void Graph::remove_edge(int id1, int id2){
  std::map<int, Graph::Node>::iterator it1 = nodes.find(id1);
  std::map<int, Graph::Node>::iterator it2 = nodes.find(id2);
  if(it1 != nodes.end() && it2 != nodes.end()){
    nodes[id1].remove_edge_to(id2);
    nodes[id2].remove_edge_to(id1);
  }
}

std::vector<Graph::Edge> Graph::get_edges(){
  std::vector<Graph::Edge> edges;
  for(const auto& n: nodes){
    int id = n.first;
    const Graph::Node& node = n.second;
    const glm::vec3& a = node.location;
    
    for(int neighbour_id: node.adjacency_list){
      if(neighbour_id < id) continue; // do not include the same edge twice
      const Graph::Node& neighbour = nodes[neighbour_id];
      const glm::vec3& b = neighbour.location;
      glm::vec3 c = (b - a);
      // the lane is offset from the center of the road by 1/4 of the road total width
      glm::vec3 left_lane_offset = glm::normalize(glm::vec3(c.z, c.y, -c.x)) * world.get_scale() * 0.25f;
      glm::vec3 right_lane_offset = glm::vec3(-left_lane_offset.x, left_lane_offset.y, -left_lane_offset.z);
      edges.push_back({.start=(a + right_lane_offset), .end=(b + right_lane_offset)});
      edges.push_back({.start=(b + left_lane_offset), .end=(a + left_lane_offset)});
    }
  }
  
  return edges;
}

void Graph::run(){
  for(auto it = nodes.begin(); it != nodes.end(); ){
    if(it->second.still_valid()){
      ++it;
    }
    else{
      it = remove_node(it);
    }
  }
}

void Graph::draw(Shader& shader){
  // different scale constants are used to compensate the not consistent 3D model origin locations
  
  glm::vec3 offset = world.get_height_vec() - glm::vec3(0.0f, world.get_scale() * 0.245f, 0.0f);
  glm::vec4 white_color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
  
  // draw edges
  glBindVertexArray(vao_edges);
  for(const auto& n: nodes){
    int id = n.first;
    const Graph::Node& node = n.second;
    glm::vec3 a = node.location;
    
    for(int neighbour_id: node.adjacency_list){
      if(neighbour_id < id) continue; // do not draw the same edge twice
      Graph::Node& neighbour_node = nodes[neighbour_id];
      glm::vec3 b = neighbour_node.location;
      glm::vec3 c = (b - a);
      float angle = glm::atan(-c.z, c.x);
      glm::mat4 model = glm::mat4(1.0f);
      model = glm::translate(model, a + (c / 2.0f) + offset);
      model = glm::rotate(model, angle, glm::vec3(0.0f, 1.0f, 0.0f));
      model = glm::scale(model, glm::vec3(glm::length(c), world.get_scale() * 0.18f, world.get_scale()));
      glm::mat4 edge_model = glm::scale(model, glm::vec3(1.0f, 0.99f, 1.0f));
      shader.setMat4("model", edge_model);
      shader.setVec4("color", color);
      glDrawArrays(GL_TRIANGLES, 0, qube_vertices.size());
      
      // draw lane division
      glm::mat4 lane_model = glm::scale(model, glm::vec3(1.0f, 0.995f, 0.06f));
      shader.setMat4("model", lane_model);
      shader.setVec4("color", white_color);
      glDrawArrays(GL_TRIANGLES, 0, qube_vertices.size());
    }
  }
  glBindVertexArray(0);
  
  // draw nodes
  glBindVertexArray(vao_nodes);
  for(auto& node: nodes){
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, node.second.location + offset);
    model = glm::scale(model, glm::vec3(world.get_scale(), world.get_scale() * 0.18f, world.get_scale()));
    shader.setMat4("model", model);
    shader.setVec4("color", node.second.color);
    glDrawArrays(GL_TRIANGLES, 0, cylinder_vertices.size());
    
    // reset the color
    node.second.color = color;
  }
  glBindVertexArray(0);
}

