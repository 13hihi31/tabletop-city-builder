
#include <Mesh.h>
#include <Shader.h>

#include <GL/glew.h>

#include <string>
#include <vector>

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures):
    vertices(vertices), indices(indices), textures(textures)
{
  setup_mesh();
}

void Mesh::draw(Shader& shader){
  unsigned int diffuse_nr = 1;
  unsigned int specular_nr = 1;
  unsigned int normal_nr = 1;
  unsigned int height_nr = 1;
  
  for(unsigned int i = 0; i < textures.size(); ++i){
    glActiveTexture(GL_TEXTURE0 + i); // activate proper texture unit before binding
    // retrieve texture number
    std::string number;
    std::string name = textures[i].type;
    if(name == "texture_diffuse")
      number = std::to_string(diffuse_nr++);
    else if(name == "texture_specular")
      number = std::to_string(specular_nr++);
    else if(name == "texture_normal")
      number = std::to_string(normal_nr++);
    else if(name == "texture_height")
      number = std::to_string(height_nr++);
      
    // now set the sampler to the correct texture unit
    glUniform1i(glGetUniformLocation(shader.ID, (name + number).c_str()), i);
    // and finally bind the texture
    glBindTexture(GL_TEXTURE_2D, textures[i].id);
  }
  
  // draw mesh
  glBindVertexArray(vao);
  glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
  
  // set everything back to defaults
  glActiveTexture(GL_TEXTURE0);
}
  
void Mesh::setup_mesh(){
  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);
  
  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
  
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
  
  // vertex positions
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
  // vertex normals
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
  // vertex texture coords
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tex_coords));
  // vertex tangent
  glEnableVertexAttribArray(3);
  glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));
  // vertex bitagent
  glEnableVertexAttribArray(4);
  glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bitangent));
  
  glBindVertexArray(0);
}

