
#include <chrono>

#include <Video.h>
#include <matToTexture.h>

#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>

#include <glm/gtc/constants.hpp>

#include <iostream>

Video::Video(int video_id):
  video_id(video_id)
{
  frames.resize(buffer_size);
  head = 0;
  tail = 0;
}

void Video::run(){
  continue_running = true;
  video_is_on = false;
  
  cv::VideoCapture capture = cv::VideoCapture(video_id);
  if(!capture.isOpened()){
    std::cout << "Cannot open video" << std::endl;
    return;
  }
  
  video_is_on = true;
  
  video_width = static_cast<float>(capture.get(cv::CAP_PROP_FRAME_WIDTH));
  video_height = static_cast<float>(capture.get(cv::CAP_PROP_FRAME_HEIGHT));
  //std::cout << video_width << " " << video_height << "\n";
  
  double fps = capture.get(cv::CAP_PROP_FPS);
  //std::cout << fps << "\n";
  if(fps != fps){ // NaN
    fps = 25.0;
  }
  auto frame_time = std::chrono::duration<double>(1.0 / fps);
  
  while(continue_running){
    auto start = std::chrono::steady_clock::now();
    
    // camera input
    if(!capture.read(frames[head])){
      std::cout << "Cannot grab a frame" << std::endl;
      break;
    }
    
    head = (head + 1) % buffer_size;
     
    auto elapsed_time = std::chrono::steady_clock::now() - start;
    if(elapsed_time < frame_time){
      auto sleep_time = frame_time - elapsed_time;
      std::this_thread::sleep_for(sleep_time);
    }
  }
}

// Function turn a cv::Mat into a texture, and return the texture ID as a unsigned int for use
// this function should be called only in a thread where there is an OpenGL context
unsigned int Video::get_video_as_texture(GLenum min_filter, GLenum mag_filter, GLenum wrap_filter){
  cv::Mat& frame = frames[tail];
  matToTexture(frame, min_filter, mag_filter, wrap_filter);
}

std::vector<Video::Marker> Video::get_marker_data(){
  // detect markers
  cv::aruco::detectMarkers(frames[tail], dictionary, marker_corners, marker_ids, parameters, rejected_candidates);
  //cv::aruco::drawDetectedMarkers(frames[tail], marker_corners, marker_ids);
  
  std::vector<Marker> marker_data(marker_corners.size());
  
  // calculate detected markers centers in the opengl coordinate system
  for(size_t i = 0; i < marker_corners.size(); ++i){
    const std::vector<cv::Point2f>& marker_corner = marker_corners[i];
    
    float center_x = 0.0f;
    float center_y = 0.0f;
    float size = 0.0f;
    for(size_t i = 0; i < marker_corner.size(); ++i){
      const cv::Point2f& corner_a = marker_corner[i];
      const cv::Point2f& corner_b = marker_corner[(i + 1) % marker_corner.size()];
      size += static_cast<float>(cv::norm(corner_a - corner_b));
      center_x += corner_a.x;
      center_y += corner_a.y;
    }
    center_x /= 4.0f;
    center_y /= 4.0f;
    size /= 4.0f;
    
    const cv::Point2f& first = marker_corner[0];
    float angle = -(glm::atan(first.y - center_y, first.x - center_x) + glm::quarter_pi<float>());
    
    // the opencv image coordinate system is different from opengl coordinate system
    center_x = 0.5f - center_x / video_width;
    center_y = (0.5f * video_height - center_y) / video_width;
    size /= video_width;
    
    // the video is drawn in the x-z plane
    marker_data[i] = {.center=glm::vec3(center_x, 0.0f, center_y), .size=size, .angle=angle, .id=marker_ids[i]};
  }
  
  return marker_data;
}

bool Video::has_new_image(){
  return head != tail;
}

void Video::gen_new_image(){ 
  tail = std::min((tail + 1) % buffer_size, head);
}

void Video::stop(){
  continue_running = false;
}

